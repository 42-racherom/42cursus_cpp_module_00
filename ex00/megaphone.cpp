/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/25 22:14:23 by rauer             #+#    #+#             */
/*   Updated: 2024/03/26 16:59:02 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

char	to_upper(char c)
{
	if (c >= 'a' && c <= 'z')
		c -= 32;
	return (c);
}

void	megaphone(char *str)
{
	int		i;
	char	buf[128];

	if (!str)
	{
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *";
		return ;
	}
	i = 0;
	while (str && *str)
	{
		buf[i++] = to_upper(*(str++));
		if (i >= 127 || !*str)
		{
			buf[i] = 0;
			std::cout << buf;
			i = 0;
		}
	}
}

int	main(int argc, char **argv)
{
	int	i;

	if (argc < 2)
		megaphone(0);
	i = 1;
	while (i < argc)
		megaphone(argv[i++]);
	std::cout << std::endl;
	return (0);
}
