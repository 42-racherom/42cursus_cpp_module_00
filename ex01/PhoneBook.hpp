/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBook.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 17:28:06 by racherom          #+#    #+#             */
/*   Updated: 2024/03/30 05:56:48 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONE_BOOK_H
# define PHONE_BOOK_H
# ifndef MAX_CONTACTS
#  define MAX_CONTACTS 8
# endif
# include "Contact.hpp"

class PhoneBook
{
  public:
	PhoneBook(void);
	~PhoneBook(void);
	void add_contact(void);
	void search_contact(void);

  private:
	int next_contact;
	Contact contacts[MAX_CONTACTS];
	void list_contacts(void);
	void show_contact(int i);
	static void print_field(const char *str);
};

#endif
