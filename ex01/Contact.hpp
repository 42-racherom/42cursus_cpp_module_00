/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 17:22:34 by racherom          #+#    #+#             */
/*   Updated: 2024/03/30 06:21:59 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTACT_H
# define CONTACT_H
# include <iostream>

class Contact
{
  public:
	Contact(void);
	~Contact(void);
	void read(void);
	void print(void);
	const char *get_first_name(void);
	const char *get_last_name(void);
	const char *get_nickname(void);

  private:
	char first_name[128];
	char last_name[128];
	char nickname[128];
	char phone_number[128];
	char secret[512];
	static void read_input(char *, const char *, size_t);
};

#endif
