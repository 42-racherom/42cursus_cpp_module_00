/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 17:43:39 by racherom          #+#    #+#             */
/*   Updated: 2024/04/18 19:50:45 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PhoneBook.hpp"
#include <cstring>
#include <iomanip>
#include <iostream>

int	main(void)
{
	PhoneBook	book;
	char		buf[8];

	while (1)
	{
		std::cout << "Select command <ADD|SEARCH|EXIT>: ";
		std::cin.getline(buf, 8);
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(1000000, '\n');
		}
		if (!std::strcmp("ADD", buf))
			book.add_contact();
		else if (!std::strcmp("SEARCH", buf))
			book.search_contact();
		else if (!std::strcmp("EXIT", buf))
		{
			std::cout << "Goodby" << std::endl;
			break ;
		}
	}
	return (0);
}
