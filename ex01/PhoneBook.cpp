/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 17:30:08 by racherom          #+#    #+#             */
/*   Updated: 2024/03/30 07:32:56 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.hpp"
#include "PhoneBook.hpp"
#include <iostream>

PhoneBook::PhoneBook(void)
	: next_contact(0)
{
}

PhoneBook::~PhoneBook(void)
{
}

void PhoneBook::add_contact(void)
{
	this->contacts[this->next_contact & 7].read();
	this->next_contact++;
	this->next_contact &= 15;
}

void PhoneBook::search_contact(void)
{
	int	i;

	i = -1;
	this->list_contacts();
	std::cout << "Select index: ";
	std::cin >> i;
	if (std::cin.fail() || (!std::cin.eof() && std::cin.peek() != '\n'))
	{
		std::cin.clear();
		std::cout << "Invalid index" << std::endl;
	}
	else if (i >= 0 && i < this->next_contact && i < MAX_CONTACTS)
		this->contacts[i].print();
	else
		std::cout << "Invalid index" << std::endl;
	std::cin.ignore(1000000, '\n');
}

void PhoneBook::list_contacts(void)
{
	int	i;

	std::cout << "     index|first_name| last_name|  nickname" << std::endl;
	i = 0;
	while (i < MAX_CONTACTS && i < this->next_contact)
	{
		std::cout << "         " << i << "|";
		this->print_field(this->contacts[i].get_first_name());
		std::cout << "|";
		this->print_field(this->contacts[i].get_last_name());
		std::cout << "|";
		this->print_field(this->contacts[i++].get_nickname());
		std::cout << std::endl;
	}
}

void PhoneBook::print_field(const char *str)
{
	char	out[11];
	size_t	i;
	size_t	l;

	l = 0;
	while (str[l] && l < 11)
		l++;
	i = 0;
	while (i + l < 10)
		out[i++] = ' ';
	l = 0;
	while (i < 10)
		out[i++] = str[l++];
	if (l >= 10 && str[l])
		out[9] = '.';
	out[10] = 0;
	std::cout << out;
}
