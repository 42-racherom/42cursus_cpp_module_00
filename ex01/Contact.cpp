/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/27 02:36:59 by racherom          #+#    #+#             */
/*   Updated: 2024/03/30 07:34:47 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.hpp"
#include <iomanip>
#include <iostream>

Contact::Contact(void)
{
}
Contact::~Contact(void)
{
}

void Contact::read_input(char *ref, const char *promt, size_t l)
{
	std::cout << "Enter " << promt << ": ";
	std::cin.getline(ref, l);
	if (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore(1000000, '\n');
	}
	while (!ref[0])
	{
		std::cout << "You have to enter a " << promt << ": ";
		std::cin.getline(ref, l);
	}
}

void Contact::read(void)
{
	this->read_input(this->first_name, "first name", 128);
	this->read_input(this->last_name, "last name", 128);
	this->read_input(this->nickname, "nickname", 128);
	this->read_input(this->phone_number, "phone number", 128);
	this->read_input(this->secret, "darkest secret", 512);
}

void Contact::print(void)
{
	std::cout << "First name: " << this->first_name << std::endl;
	std::cout << "Last name: " << this->last_name << std::endl;
	std::cout << "Nickname: " << this->nickname << std::endl;
	std::cout << "Phone number: " << this->phone_number << std::endl;
	std::cout << "Darkest secret: " << this->secret << std::endl;
}

const char *Contact::get_first_name(void)
{
	return (this->first_name);
}

const char *Contact::get_last_name(void)
{
	return (this->last_name);
}

const char *Contact::get_nickname(void)
{
	return (this->nickname);
}
